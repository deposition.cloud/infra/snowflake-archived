# Snowflake

WSL-friendly ansible playbooks to bootstrap a personal high-availability Microk8s cluster.

And since we're at it, why not also provision the Windows host?

More nodes, better automation.

## Control Node Prep

Assuming a Ubuntu machine/VM (here WSL2), let's first setup the control node from which ansible playbooks will get run.

1. Install Ansible

    ``` bash
    sudo apt update
    sudo apt install software-properties-common ansible -y
    ```

1. Clone this repo

    ``` bash
    # mkdir ~/code/
    git clone https://gitlab.com/deposition.cloud/infra/snowflake.git
    ```

If repo already checked out locally:

``` bash
ansible-playbook local.yml -K
```

Run only tagged tasks

``` bash
ansible-playbook local.yml -K --tags keybase
```

## Use

Install collections and 3rd party roles.

``` bash
ansible-galaxy collection install -r requirements.yaml
ansible-galaxy role install -r requirements.yaml --roles-path roles/
```

For passphrase protected keys: `ssh-agent`; even better via [keychain](https://www.funtoo.org/Keychain)

``` bash
ssh-agent bash
ssh-add ~/.ssh/id_rsa
```

Assuming same sudo password on *all* targeted machines.

``` bash
ansible-playbook rebase.yaml -i inventory.yaml -K
```

Should now see something like:

``` bash
...
PLAY RECAP **********************************************************************************************************
node1                      : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
node2                      : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
primary                    : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

## Reading

* [How To Install and Configure Ansible on Ubuntu 20.04](https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-ansible-on-ubuntu-20-04)
* [Ansible and HashiCorp: Better Together](https://www.hashicorp.com/resources/ansible-terraform-better-together)
* [How to manage your workstation configuration with Ansible](https://opensource.com/article/18/3/manage-workstation-ansible)

## License

Ethically sourced under the [Atmosphere License](https://www.open-austin.org/atmosphere-license/)—like open source, for good.
