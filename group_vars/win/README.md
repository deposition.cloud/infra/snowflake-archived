# Windows Provisioning

Assuming WinRM has set up OpenSSH.

``` bash
ansible-playbook win.yaml -K --vault-password-file /usr/local/bin/op-vault --inventory inventory.yaml
```

Debug with:

``` bash
ansible-playbook win.yaml -K --vault-password-file /usr/local/bin/op-vault --inventory inventory.yaml -vvvv --tags debug
```
