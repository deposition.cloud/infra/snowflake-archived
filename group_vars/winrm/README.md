# Windows Host Prep

Background reading: [User Guide » Windows Guides » Setting up a Windows Host » WinRM Setup](https://docs.ansible.com/ansible/latest/user_guide/windows_setup.html#winrm-setup)

First, let's get WinRM working, later we install OpenSSH.

``` powershell
$url = "https://raw.githubusercontent.com/ansible/ansible/devel/examples/scripts/ConfigureRemotingForAnsible.ps1"
$file = "$env:temp\ConfigureRemotingForAnsible.ps1"

(New-Object -TypeName System.Net.WebClient).DownloadFile($url, $file)

powershell.exe -ExecutionPolicy ByPass -File $file
```

``` powershell
# Remove HTTP WinRM listeners
Get-ChildItem -Path WSMan:\localhost\Listener | Where-Object { $_.Keys -contains "Transport=HTTP" } | Remove-Item -Recurse -Force
```

Should only see the HTTPS WinRM listener:

``` powershell
winrm enumerate winrm/config/Listener
```

## WinRM to OpenSSH

Assuming control node has already been localhost provisioned.

To log in to 1Password via the CLI, as per [pcuci/dotfiles](https://github.com/pcuci/dotfiles/blob/main/.bash_aliases).

```bash
op-in
```

Use 1Password vault password file to encrypt your Windows administrator password.

``` bash
ansible-vault encrypt_string --vault-password-file /usr/local/bin/op-vault --stdin-name "vault_ansible_password"
```

Copy paste output to `vault` file in this folder.

Debugging should print the plain text password.

``` bash
ansible-playbook winrm.yaml -K --vault-password-file /usr/local/bin/op-vault --inventory inventory.yaml -vvvv --tags debug
```

Now the magic. Provision!

``` bash
ansible-playbook winrm.yaml -K --vault-password-file /usr/local/bin/op-vault --inventory inventory.yaml
```

## Troubleshooting

Ensure port 22 is not blocked by the Windows firewall.
